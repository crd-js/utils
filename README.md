# Utilities

CRD utilities extends ```Function.prototype``` and features a few helpers thru ```CRD.Utils```.

### Function.prototype

#### Function.prototype.debounce

```Function.prototype.debounce``` allows to "debounce" function execution. This is helpful for events that triggers several times in a short period of time (like ```window.scroll```). Using ```debounce``` the passed function could be executed only at certain interval, despite of how many times the event is triggered.

##### Arguments

```Function.prototype.debounce``` accepts two arguments:

- ```wait``` - Time interval for function execution.
- ```inmediate``` - default to ```false``` - Set to ```true``` if function needs to be executed right away.

##### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="function.js"></script>
```

The example will hook a function to the ```window.scroll``` event, but will only get executed at an interval of 250 ms.

```javascript
jQuery(window).scroll(function() {
	console.log('Window scroll!');
}.debounce(250));
```

#### Function.prototype.once

```Function.prototype.once``` allows to execute a recursive function just once (for example a function called by an event).

##### Arguments

```Function.prototype.debounce``` accepts a single argument:

- ```context``` - Context where the function will be applied.

##### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="function.js"></script>
```

The example will hook a function to the ```window.scroll``` event, but will only trigger once.

```javascript
jQuery(window).scroll(function() {
	console.log('Window scroll!');
}.once());
```

#### Function.prototype.inherit

```Function.prototype.inherit``` allows inheritance between ```javascript``` functions (classes).

##### Arguments

```Function.prototype.inherit``` accepts a single argument:

- ```object``` - Object from where the current function will inherits all its properties.

##### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="function.js"></script>
```

The example create a constructor. Then will create another object that will inherits from the first one.

```javascript
// Creates the first constructor
function Human(name) {
	this.name = name;
};
Human.prototype.sayYourName = function() {
	console.log(this.name);
};

// Creates a Human
var luke = new Human('Luke');
luke.sayYourName(); // Outputs to console 'Luke'

// Extends Human
function Man(name) {
	this.name = name;
};
Man.inherit(Human);

// Creates a new Man
var father = new Man('Vader');
father.sayYourName(); // Outputs to console 'Vader'
```

#### Function.prototype.bind

```Function.prototype.bind``` allows to apply (or bind) a function to a specific object.

##### Arguments

```Function.prototype.bind``` accepts two arguments:

- ```object``` - Context where the function will be applied.
- ```args``` - Function arguments (as an ```array```).

##### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="function.js"></script>
```

The example will hook a function to the ```window.scroll``` event, but will only trigger once.

```javascript
// Creates an object to bind a function to it
var o = {
	foo : 'Bar'
};

// Creates a function and binds the function to the previously created object
var binded = function() {
	console.log(this.foo);
}.bind(o);

// Executes the function
binded(); // Outputs to console 'Bar'

```

### CRD.Utils

#### CRD.Utils.setOptions

```CRD.Utils.setOptions``` is a helper specifically created for using with ```javascript``` objects (functions and classes). ```setOptions``` will set object options, and will inherit properties for a default set of options if specified. It will also hook any events defined as in the patters shown in the example (defined in a property ```on``` in the options object; the property must contain an object, containing a set of key and values, where key defines the event name, and the value defines the function to be executed).

##### Arguments

```CRD.Utils.setOptions``` accepts three arguments:

- ```object``` - Context where the options will be stored (in object.options).
- ```options``` - Object containing the options.
- ```defaults``` - Object containing the default values for the options.

##### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="utils.js"></script>
```

The example will hook a function to the ```window.scroll``` event, but will only get executed at an interval of 250 ms.

```javascript
// Creates the constructor
function LivingThing(options) {
	CRD.Utils.setOptions(this, options, this.defaults);
};
LivingThing.prototype = {
	defaults : {
		eyes : 2,
		legs : 2,
		arms : 2
	},
	fire : function() {
		jQuery(this).trigger('customevent', [
			this.options.eyes, 
			this.options.legs, 
			this.options.arms
		]);
	}
};

// Creates a new living thing
var snake = new LivingThing({
	legs : 0,
	arms : 0,
	on   : {
		'customevent' : function(event, eyes, legs, arms) {
			console.log(eyes, legs, arms);
		}
	}
});

// Triggers the custom event
snake.fire(); // Outputs to console 2, 0, 0
```

#### CRD.Utils.setConstants

```CRD.Utils.setConstants``` will set a constant like variables to a specified object using ```Object.defineProperty```.

##### Arguments

```CRD.Utils.setConstants``` accepts two arguments:

- ```object``` - Context where the constants will be setted.
- ```constants``` - Object containing the constants to be set. It will recursively define all the properties in the nested objects as object constants.

##### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="utils.js"></script>
```

The example will hook a function to the ```window.scroll``` event, but will only get executed at an interval of 250 ms.

```javascript
// Creates an object
var o = {
	foo : 'Bar'
};

// Defines the constants
CRD.Utils.setConstants(o, {
	Constants : {
		ONE : 'One',
		TWO : 'Two'
	}
});

// Try to change a constant value
console.log(o.Constants.ONE); // Outputs to console 'One'
o.Constants.ONE = 'Changed!';
console.log(o.Constants.ONE); // Outputs to console 'One'
```

#### CRD.Utils.defineHelper

```CRD.Utils.defineHelper``` will define a ```jQuery``` helper function to specifically initialize objects (functions or classes) instances. The objects must accespt as first argument an element or ```jQuery``` object reference. This helper also provides instance storage (for the element, so the object wont be initialized twice for the same element) and a way to get the instances, and also call object methods.

##### Arguments

```CRD.Utils.setConstants``` accepts two arguments:

- ```object``` - Object (function or class) to be initialized when the helper gets called.
- ```helper``` - Helper name (to use as jQuery(element).helper(arguments)).
- ```storage``` - Key for the storing the initialized object in element storage. If no storage name is supplied, helper name will be used as default.

##### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="utils.js"></script>
```

The example will hook a function to the ```window.scroll``` event, but will only get executed at an interval of 250 ms.

```javascript
// Creates a constructor
function domElement(element) {
	this.element = jQuery(element);
};
domElement.prototype.getTag = function() {
	return this.element.tagName;
};
CRD.Utils.defineHelper(domElement, 'domElement');

// Creates a new domElement
jQuery('div').domElement();

// Example usage
console.log(jQuery('div').domElement('get') === jQuery('div')); // Outputs to console 'true'
console.log(jQuery('div').domElement('getTag')); // Outputs to console 'DIV'
```

### Dependencies

- JQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
